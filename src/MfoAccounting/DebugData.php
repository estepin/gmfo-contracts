<?php
namespace MfoRu\Contracts\MfoAccounting;

interface DebugData
{
    function getRequestHeader():string;

    function getRequestBody():string;

    function getResponseHeader():string;

    function getResponseBody():string;
}