<?php
namespace MfoRu\Contracts\MfoAccounting;

interface Connector
{
    const STATUS_RECEIVED = 1; //Анкета принята
    const STATUS_ISSUED = 2; //Выдан займ
    const STATUS_REJECT = 3; // Отказ
    const STATUS_ERROR = 4; // Ошибка
    const STATUS_APPROVED = 5; //Займ одобрен
    const STATUS_SENDED = 6; //Анкета отправлена

    function addToUpload(Anket $anket, $config);

    //Должен вернуть одну из констант STATUS_
    function checkStatus($anketId, $config);

    function testConfig($config):bool;

    function getAccData($id);

    function getConfigModel();

    function getDebugData():DebugData;
}