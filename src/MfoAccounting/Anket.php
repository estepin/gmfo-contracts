<?php
namespace MfoRu\Contracts\MfoAccounting;

class Anket
{
    public $mfoId;

    public $summ;
    public $term;

    public $typeIssuance;
    public $typeDeposit;

    public $firstname;
    public $middlename;
    public $lastname;
    public $birthdate;
    public $gender;

    public $phone;
    public $email;

    public $passSeriaNum;
    public $passDate;
    public $passWhoIssued;
    public $passBirthPlace;
    public $passCode;

    public $regIndex;
    public $regRegion;
    public $regCity;
    public $regStreet;
    public $regHouse;
    public $regBuild;
    public $regRoom;
    public $regOkato;

    public $liveIndex;
    public $liveRegion;
    public $liveCity;
    public $liveStreet;
    public $liveHouse;
    public $liveBuild;
    public $liveRoom;
    public $liveOkato;

    public $saId;
    public $saStatus;

    const CRM_STATUS_APPROVED = 'accept';
    const CRM_STATUS_ISSUED = 'issued';
    const CRM_STATUS_REJECT = 'reject';

    function check()
    {
        $essential = ['mfoId'];
        foreach ($essential as $f)
        {
            if(!$this->$f)
            {
                throw new \Exception('Anket is wrong');
            }

        }
    }
}