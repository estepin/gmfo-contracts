<?php
namespace MfoRu\Contracts\Billing;

interface Service
{
    function getEmptyTransaction():Transaction;

    function commitTransactions();

    function addTransaction(Transaction $transaction);

    function rollbackTransactions();

    function calculateBalance($accId);

    function getAccount($accId);
}