<?php
namespace MfoRu\Contracts\Billing;

interface Transaction
{
    function rollback();

    function commit();
}